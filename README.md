This repository is created for weekly secions of Python scripting course at 
IASBS, Zanjan, I.R.Iran.
The texts are :
1. Numerical Python A Practical Techniques Approach for Industry, Robert Johansson
2. A Student's Guide to Python for Physical Modeling, Jesse M. Kinder & Philip Nelson
3. A Primer on Scientific Programming with Python-Hans Petter Langtangen 4th Ed.(2014) 
4. Python Scripting for Computational Science - Hans Petter Langtangen 3rd Edition


The slides and codes are avalable from here:
http://hplgit.github.io/scipro-primer/

http://folk.uio.no/hpl/scripting/

Take a look at the Examples:

http://hplgit.github.io/primer.html/doc/pub/class/._class-readable000.html#table_of_contents


To install the required packages use the following:

** Ubuntu & Debian **

sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose

Or

** Windows, Mac and linux**

https://www.anaconda.com/download/